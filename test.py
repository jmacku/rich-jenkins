#!/usr/bin/env python

CONST = 123

def my_func(divider):
    return (CONST / divider)

if __name__ == '__main__':
    print(my_func(123))
    print(my_func(246))
    print(my_func(0))

