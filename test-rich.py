#!/usr/bin/env python

from rich.console import Console
from rich.traceback import install
my_console = Console(force_terminal=True)
install(console=my_console, show_locals=True)

CONST = 123

def my_func(divider):
    return (CONST / divider)

if __name__ == '__main__':
    print(my_func(123))
    print(my_func(246))
    print(my_func(0))

